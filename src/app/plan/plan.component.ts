import {Component, OnInit} from '@angular/core';
import roomsData from './../../assets/room.config.json';
import {Room} from '../model/room';
import {HttpClient} from '@angular/common/http';
import {ZoomService} from '../zoom.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './plan.component.html',
  styleUrls: ['./plan.component.scss']
})

export class PlanComponent implements OnInit {
  constructor(private zoomService: ZoomService) {
  }

  rooms: Array<Room> = roomsData;

  ngOnInit() {
    this.rooms.forEach(room => {
      room.participants = [];
      this.zoomService.getParticipants(room.meetingId)
        .subscribe(data => {
            room.participants = data.participants;
          },
          error => console.log('Error!', error));
    });
  }
}
